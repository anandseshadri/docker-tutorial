## How to save a docker container's state

The data doesn’t persist when that container ceases to exists, and it can be difficult to get the data out of the container if another process needs it.
Docker has two options for containers to store files in the host machine, so that the files are persisted even after the container stops: **volumes** and **bind mounts**.If you’re running Docker on Linux you can also use a **tmpfs mount**.

An easy way to visualize the difference among volumes, bind mounts, and tmpfs mounts is to think about where the data lives on the Docker host.


![SaveContainerState](https://docs.docker.com/storage/images/types-of-mounts.png)


**Method-1**

This way the Container data/ container State can be saved after the container is created...unlike the next two ways mentioned below.
But this NOT the prefered way.  (Size of newly saved image will be more !) 

The usual way is at least through a docker commit; that will freeze the state of your container into a new image.
This is not the best practice, and using a Dockerfile allows you to formally model the image content and ensure you can rebuild/reproduce its initial state.

```commandline
docker ps
```
```commandline
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS              NAMES
c3f279d17e0a        ubuntu:12.04        /bin/bash           7 days ago          Up 25 hours                            desperate_dubinsky
197387f1b436        ubuntu:12.04        /bin/bash           7 days ago          Up 25 hours                            focused_hamilton
```
Using the ContainerID perform docker commit

- `docker commit c3f279d17e0a  svendowideit/testimage:version3`

The output is the ID of the newly created image **f5283438590d**

check your repository for the new image. Also note its size:

```commandline
docker images
```
```commandline
REPOSITORY                        TAG                 ID                  CREATED             SIZE
svendowideit/testimage            version3            f5283438590d        16 seconds ago      335.7 MB
```


**Method-2**

Create **docker volume** to save container data. Volume has to be created before creating container!!

Choose the right type of mount!!
read about it [here](https://docs.docker.com/storage/)

- **Direct Mount (Bind Mount)**

Contents inside Docker directory path is saved to a host directory path (in host file system)

**create docker volume to save container data (Volume created in HOST os file system)**

```commandline
sudo docker volume ls
```
```commandline
sudo docker volume create --name volume1
```
```commandline
sudo docker volume inspect volume1
```

- `docker run -tdi --name testvolcont1 -v /home/admin/DockerVolumes:/home/Samples/CygNet cygnetimage bash`
- `docker exec -it testvolcont1 bash`

- **Volumes**

First create a volume in your local

create docker volume to save container data as shown above (Volume created in HOST os file system)

- `docker run --rm -i -v test:/tmp/testvol busybox find /tmp/testvol`


Read more about it [here](https://docs.docker.com/storage/volumes/)





